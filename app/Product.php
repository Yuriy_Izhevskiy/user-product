<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Product extends Model
{
    protected $fillable = [
        'status',
        'name',
        'category_id',
        'size',
        'price',
    ];
    protected  $hidden = [];

    const sortable = ['id', 'name', 'size', 'price'];
    const countRecords = ['2','10','15','20'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function getAvailabilityAttribute()
    {
        return $this->status ? 'Availability' : 'Not available';
    }

    public function scopeSearch($query, $q, string $searchable)
    {

        if($q == null) {
            return $query;
        }

        return $query
            ->where($searchable, 'LIKE', "%{$q}%");
    }
}
