<?php

namespace App\Http\Requests\product;

use Illuminate\Foundation\Http\FormRequest;

class FieldRequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|min:3',
            'price' => 'required|numeric',
            'category_id' => 'required|numeric',
            'size' => 'numeric',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'An Name is required',
            'price.required'  => 'The price is required',
            'category_id.required'  => 'The category is required',
            'size.required'  => 'The size is a numeric type',
        ];
    }
}
