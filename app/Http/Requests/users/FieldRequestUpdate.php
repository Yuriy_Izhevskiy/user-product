<?php

namespace App\Http\Requests\users;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FieldRequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = request()->user();
        return [
            'name' => 'required|min:3|max:50',
            'last_name' => 'required|min:3|max:50',
            'email' => [
                'required',
                Rule::unique('users')->ignore($user),
            ],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required',
            'last_name.required' => 'Last name is required',
            'email.required' => 'We need to know your e-mail address!',
        ];
    }
}
