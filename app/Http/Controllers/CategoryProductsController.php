<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;

class CategoryProductsController extends Controller
{

    public function show(Category $category)
    {
        $product = $category->product;

        return view('category.show', ['products' => $product, 'categoryName' => $category->name]);
    }
}
