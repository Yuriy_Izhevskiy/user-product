<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    protected $prefix = 'dashboard';

    public function index()
    {

        $categories = Category::count();
        $categoriesDisabled  = Category::whereStatus(false)->count();
        $categoriesActive   = Category::whereStatus(true)->count();

        $products = Product::count();
        $productsDisabled = Product::whereStatus(false)->count();
        $productsActive = Product::whereStatus(true)->count();

        return view($this->prefix.'.index', [
            'products' => $products,
            'productsDisabled'=> $productsDisabled,
            'productsActive' => $productsActive,
            'categories' => $categories,
            'categoriesDisabled' => $categoriesDisabled,
            'categoriesActive' => $categoriesActive,
        ]);
    }
}
