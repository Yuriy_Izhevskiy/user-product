<?php

namespace App\Http\Controllers;

use App\Http\Requests\users\FieldRequestUpdate;
use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::all();

        return view('user.index', ['users' => $users]);
    }

    public function show($id)
    {
        $user = User::find((int)$id);
        return view('user.show', ['user' => $user] );
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('user.edit', ['user' => $user] );
    }

    public function update(FieldRequestUpdate $request, $id)
    {
        $validationData = $request->validated();
        User::whereId($id)->update($validationData);

        return redirect('/users')->with('success', $request->name . ' profile updated successfully');
    }
}
