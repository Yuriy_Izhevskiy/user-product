<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\product\FieldRequestCreate;
use App\Http\Requests\product\FieldRequestUpdate;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use PhpParser\Builder;

class ProductsController extends Controller
{

    public function index(Request $request, Product $product)
    {
        $categoryIdSelected = [];
        if ($request->has('category_id')) {
            $categoryIdSelected = $request->query('category_id');
        }
        $productsDates = $this->formFilter($request, $categoryIdSelected);

        return view('product.index',
            [
                'products' => $productsDates['products'],
                'orderBy' => $productsDates['orderBy'],
                'sortBy' => $productsDates['sortBy'],
                'q' => $productsDates['q'],
                'perPage' => $productsDates['perPage'],
                'sortable' => Product::sortable,
                'countRecords' => Product::countRecords,
                'categories' => Category::all(),
                'categoryIdSelected' => $categoryIdSelected
        ]);
    }

    public function formFilter($request, $categoryIdSelected = [])
    {
        $sortBy = 'id';
        $orderBy = 'desc';
        $perPage = 5;
        $q = null;

        if ($request->has('orderBy')) $orderBy = $request->query('orderBy');
        if ($request->has('price_start')) $price['price_start'] = $request->query('price_start');
        if ($request->has('price_end')) $price['price_end'] = $request->query('price_end');

        if ($request->has('sortBy')) $sortBy = $request->query('sortBy');
        if ($request->has('perPage')) $perPage = $request->query('perPage');
        if ($request->has('q')) $q = $request->query('q');

        $products = Product::with(['category'])

            ->whereIn('category_id', $categoryIdSelected)
            ->search($q, $sortBy)
            ->orderBy($sortBy, $orderBy)
            ->paginate($perPage)
            ->appends(request()->except('page'));

        return [
            'products' => $products,
            'orderBy' => $orderBy,
            'sortBy' => $sortBy,
            'q' => $q,
            'perPage' => $perPage,
            'selectedCategoryId' => $categoryIdSelected
        ];
    }

    public function create()
    {
        $categories = Category::select('id', 'name')->get();

        return view('product.create', ['categories' => $categories]);
    }

    public function store(FieldRequestCreate $request)
    {
        $validationData = $request->validated();
        Product::create($validationData);

        return redirect('/products')->with('success', $request->name . ' is successfully create');
    }

    public function edit(Product $product)
    {
        $categories = Category::select('id', 'name')->get();

        return view('product.edit', ['product' => $product, 'categories' => $categories]);
    }

    public function update(FieldRequestUpdate $request, Product $product)
    {
        $product->update($request->all());

        return redirect('/products')->with('success',  $request->name . ' is successfully updated');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect('/products')->with('success', 'Product is successfully deleted');
    }

    public function show()
    {
        $products = Product::all();
        return view('product.show', ['products' => $products]);
    }
}
