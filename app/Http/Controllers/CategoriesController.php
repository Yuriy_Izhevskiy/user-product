<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\category\FieldRequestCreate;
use App\Http\Requests\category\FieldRequestUpdate;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Category::class, 'category');
    }

   public function index(User $user)
   {
       //Role::create(['name' => 'regular']);
       //$permission = Permission::create(['name' => 'show products']);
//
       //$role = Role::findById(2);
//       //$permission  = Permission::findById(9);
       //$role->givePermissionTo($permission);
      // dd($ers);

//auth()->user()->givePermissionTo('create products');
//auth()->user()->assignRole('regular');

//       return auth()->user()->getAllPermissions();

       //return User::role('admin')->get();

//       dd($user->hasRole('admin'));
       $categories = Category::all();
       return view('category.index', ['categories' => $categories]);
   }

   public function create()
   {
       return view('category.create');
   }

    public function store(FieldRequestCreate $request)
    {
        $validationData = $request->validated();
        Category::create($validationData);

        return redirect('/categories')->with('success', $request->name . ' category is successfully updated');
    }

    public function edit(Category $category)
    {
        return view('category.edit', ['category' => $category]);
    }


    public function update(FieldRequestUpdate $request, Category $category)
    {
        $validationData = $request->validated();
        Category::whereId($category->id)->update($validationData);

        return redirect('/categories')->with('success',  $request->name . ' is successfully updated');
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return redirect('/categories')->with('success', 'Categories is successfully deleted');
    }

}
