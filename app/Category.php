<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Category extends Model
{
    use HasRoles;

    protected $fillable = [
        'name', 'status'
    ];

    public function product()
    {
        return $this->hasMany(Product::class);
    }

}
