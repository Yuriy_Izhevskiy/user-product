<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css">
   <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        body{
            background: #ececec;
        }
        img {
            height: auto;
            max-width: 100%;
        }
        .content{
            margin-top: 50px;
        }
        .product {
            background: #fff none repeat scroll 0 0;
            border: 1px solid #c0c0c0;
            height: 390px;
            overflow: hidden;
            padding: 25px 15px;
            position: relative;
            text-align: center;
            transition: all 0.5s ease 0s;
            margin-bottom: 20px;
        }
        .product:hover {
            box-shadow: 0 0 16px rgba(0, 0, 0, 0.5);
        }
        .product-img {
            height: 200px;
        }
        .product-title a {
            color: #000;
            font-weight: 500;
            text-transform: uppercase;
        }
        .product-desc {
            max-height: 60px;
            overflow: hidden;
        }
        .product-price {
            bottom: 15px;
            left: 0;
            position: absolute;
            width: 100%;
            color: #d51e08;
            font-size: 18px;
            font-weight: 500;
        }
        .product-size {
            bottom: 40px;
            left: 0;
            position: absolute;
            width: 100%;
            color: #d51e08;
            font-size: 18px;
            font-weight: 500;
        }

        .is-danger {
            border-color: #ff3860;
        }

        view-container {
            background: white;
            display: grid;
            grid-gap: 1px;
            grid-template-columns: minmax(20ch, 20vw) minmax(40ch, auto);
            grid-template-areas:
                    "hd hd"
                    "sd bd"
                    "ft ft";
        }

        view-container > * {
            background: white;
            padding: 1rem;
        }

        view-head {
            grid-area: hd;
        }

        view-controls {
            grid-area: sd;
        }

        view-content {
            grid-area: bd;
        }

        view-foot {
            grid-area: ft;
        }


        product-grid {
            display: grid;
            grid-template-columns: repeat(auto-fit, minmax(10ch, 20ch));
            grid-gap: 1rem;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">

                <a class="navbar-brand" href="{{{ URL::route('dashboard') }}}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                    Dashboard  </a>

                <a class="navbar-brand" href="{{{ URL::route('categories') }}}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                    categories  </a>

                <a class="navbar-brand" href="{{{ URL::route('products') }}}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
                    Product | </a>

                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
