@extends('layouts.app')

@section('content')


    <main role="main" class="container">
        <div class="d-flex align-items-center p-3 my-3 text-white-50 rounded shadow-sm" style="background-color: #6f42c1">
            <div class="lh-100">
                <h6 class="mb-0 text-white lh-100">Dashboard</h6>
                <small>Since 2019</small>
            </div>
        </div>

        <div class="my-3 p-3 bg-white rounded shadow-sm">
            <div class="card-body">
                <div class="row row-sm">
                    <div class="col-6 col-lg-3">
                        <label class="az-content-label">Products</label>
                        <div class="panel-body">
                            <table class="table table-condensed table-striped table-bordered table-hover">
                                <tbody><tr>
                                    <th>Всего</th>
                                    <td>{{$products}}</td>
                                </tr>
                                <tr>
                                    <th>Опубликованых</th>
                                    <td>{{$productsDisabled}}</td>
                                </tr>
                                <tr>
                                    <th>Не опубликованых</th>
                                    <td>{{$productsDisabled}}</td>
                                </tr>
                                </tbody></table>
                        </div>

                    </div><!-- col -->

                    <div class="col-6 col-lg-3 mg-t-20 mg-lg-t-0">
                        <label class="az-content-label">Categories</label>
                        <div class="panel-body">
                            <table class="table table-condensed table-striped table-bordered table-hover">
                                <tbody><tr>
                                    <th width="150px">Всего</th>
                                    <td>{{$categories}}</td>
                                </tr>
                                <tr>
                                    <th>Опубликованых</th>
                                    <td>{{$categoriesActive}}</td>
                                </tr>
                                <tr>
                                    <th>Не опубликованых</th>
                                    <td>{{$categoriesDisabled}}</td>
                                </tr>
                                </tbody></table>
                        </div>
                    </div><!-- col -->

                </div><!-- row -->
            </div>
        </div>

    </main>


@endsection