@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="card uper">
        <div class="card-header">
            Add category
        </div>
        <div class="card-body">

            <form method="post" action="/categories">
                <div class="form-group">
                    @csrf
                    <label for="name">Name:</label>
                    <input type="text" class="form-control {{$errors->has('name') ? 'is-danger' : ''}}" name="name" value="{{old('name')}} ">
                </div>

                <button type="submit" class="btn btn-primary">Create</button>
            </form>
        </div>
        @include('errors')
    </div>
    </div>
@endsection