@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card uper">
            <div class="card-header">
                Add category
            </div>
            <div class="card-body">

                <form method="POST" action="/categories/{{$category->id}}">
                    <div class="form-group">
                        @csrf
                        @method('PATCH')
                        <label for="name">Name:</label>
                        <input type="text" class="input {{$errors->has('title') ? 'is-danger' : ''}}" name="name" placeholder="Title" value="{{$category->name}}">
                    </div>

                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
            @include('errors')
        </div>
    </div>
@endsection