@extends('layouts.app')

@section('content')
    @include('success')
    <main role="main" class="container">
        <div class="d-flex align-items-center p-3 my-3 text-white-50 rounded shadow-sm" style="background-color: #6f42c1">
            <div class="lh-100">
                <h6 class="mb-0 text-white lh-100">Categories</h6>
                <small>Since 2019</small>
            </div>
        </div>

        <small class="d-block text-right mt-3">
            <a href="/products">Shows products of all categories</a>
        </small>

        <div class="my-3 p-3 bg-white rounded shadow-sm">

            <div class="d-flex justify-content-between align-items-center w-100">
                <h6 class="border-bottom border-gray pb-2 mb-0">Categories list</h6>
                @role('admin')
                    <a href="/categories/create"><strong class="text-gray-dark">Add new category</strong></a>
                @endrole
            </div>
            @foreach($categories as $category)
                <div class="media text-muted pt-3">
                    <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <div class="d-flex justify-content-between align-items-center w-100">
                            <a href="/category-products/{{$category->id}}"><strong class="text-gray-dark"> {{$category->name}}</strong></a>

                            @role('admin')
                                <form method="POST" action="/categories/{{$category->id}}">
                                    @method('DELETE')
                                    @csrf
                                    <div class="field">
                                        <div class="control">
                                            <button type="submit" class="button">
                                                <a href="/categories/{{$category->id}}/edit">Edit</a>
                                            </button>
                                            <button type="submit" class="button is-dark">Delete</button>
                                        </div>
                                    </div>
                                </form>
                            @endrole
                        </div>


                    </div>
                </div>
            @endforeach
        </div>
    </main>


@endsection
