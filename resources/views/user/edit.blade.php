@extends('layouts.app')

@section('content')

    <form method="POST" action="{{ route('users.update', $user->id) }}" style="margin-bottom: 1em">

        @csrf
        @method('PATCH')
        <div class="container">
            <div class="py-5 text-center">
                <img class="d-block mx-auto mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
                <h2>{{$user->name}}</h2>
            </div>

            <div class="row">
                <div class="col-md-4 order-md-2 mb-4">
                    <h4 class="d-flex justify-content-between align-items-center mb-3">
                        <span class="text-muted">Selected products</span>
                        <span class="badge badge-secondary badge-pill">3</span>
                    </h4>
                    <ul class="list-group mb-3">

                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0">Product name</h6>
                                <small class="text-muted">Brief description</small>
                            </div>
                            <span class="text-muted">$12</span>
                        </li>

                        <li class="list-group-item d-flex justify-content-between">
                            <span>Total (USD)</span>
                            <strong>$20</strong>
                        </li>
                    </ul>
                </div>


                <div class="col-md-8 order-md-1">
                    <form class="needs-validation" novalidate="">
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="firstName">First name</label>
                                <input type="text" class="form-control {{$errors->has('name') ? 'is-danger' : ''}}" name="name"  value="{{$user->name}}">
                                <div class="invalid-feedback">
                                    Valid first name is required.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="lastName">Last name</label>
                                <input type="text" class="form-control {{$errors->has('last_name') ? 'is-danger' : ''}}" name="last_name"  value="{{$user->last_name}}">
                                <div class="invalid-feedback">
                                    Valid last name is required.
                                </div>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="email">Email <span class="text-muted">(Optional)</span></label>
                            <input type="email" class="form-control {{$errors->has('email') ? 'is-danger' : ''}}" name="email"  value="{{$user->email}}">
                            <div class="invalid-feedback">
                                Please enter a valid email address for shipping updates.
                              </div>
                        </div>

                        <div class="text-success">
                            <small class="text-muted">Дата создания</small> {{$user->created_at}}

                        </div>
                        <div class="text-success">
                            <small class="text-muted">Дата последнего обновления</small> {{$user->updated_at}}
                        </div>


                        <hr class="mb-4">
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </form>
    @include('errors')
@endsection