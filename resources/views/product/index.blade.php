@extends('layouts.app')

@section('content')
    @include('success')
    <view-container>
        <view-head>
            <form action="{{ route('products') }}">
                <div class="row">
                    <div class="col-md-2" id="q">
                        <br>
                        <input class="form-control form-control-sm" type="search" name="q" value="{{ $q }}">
                    </div>
                    <div class="col-md-2 col-3">
                        Поле
                        <select name="sortBy" class="form-control form-control-sm" value="{{ $sortBy }}">
                            @foreach($sortable as $col)
                                <option @if($col == $sortBy) selected @endif value="{{ $col }}">{{ ucfirst($col) }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-2 col-3">
                        Сортировать по :
                        <select name="orderBy" class="form-control form-control-sm" value="{{ $orderBy }}">
                            @foreach(['asc', 'desc'] as $order)
                                <option @if($order == $orderBy) selected @endif value="{{ $order }}">{{ ucfirst($order) }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-2 col-3">
                        Количество записей:
                        <select name="perPage" class="form-control form-control-sm" value="{{ $perPage }}">
                            @foreach($countRecords as $page)
                                <option @if($page == $perPage) selected @endif value="{{ $page }}">{{ $page }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2 col-3">
                        <br>
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>
                <br>
            <view-controls>
                <div style="min-width: 20ch;">
                    <h6>Filter by Category</h6>
                    <ul class="list-unstyled">

                        @foreach($categories as $category)
                        <li class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" name="category_id[]" class="form-check-input" {{in_array($category->id, $categoryIdSelected) ? 'checked' : ''}} value="{{$category->id}}">{{$category->id}}
                                <span>{{$category->name}}</span>
                            </label>
                        </li>

                        @endforeach

                    </ul>
                </div>
            </view-controls>
            </form>
        </view-head>


        <view-content>
            <h6>8 results</h6>
            <div class="card">
                    <div class="table-responsive">
                        @can('create products')
                            <button type="submit"><a href="/products/create">Add new product</a></button>
                        @endcan


                        <table class="table table-striped table-sm">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>
                                    Category name</th>
                                <th>Name</th>
                                <th>Size</th>
                                <th>Price</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($products as $product)
                                <tr>
                                    <td>{{$product->id}}</td>
                                    <td>{{$product->category->name}}</td>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->size}}</td>
                                    <td>{{$product->price}}</td>
                                    <td>  <a href="/products/{{$product->id}}/edit">Edit</a></td>
                                    <td> <a href="/products/{{$product->id}}">Delete</a></td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                        <div class="clearfix">
                            {!! $products->links() !!}

                        </div>
                    </div>
                </div>
        </view-content>
    </view-container>

@endsection