@extends('layouts.app')

@section('content')

    <form method="POST" action="/products/{{$product->id}}" style="margin-bottom: 1em">

        @csrf
        @method('PATCH')
        <div class="container">
            <div class="py-5 text-center">
                <img class="d-block mx-auto mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
                <h2>Create product</h2>
            </div>

            <div class="row">

                <div class="col-md-10 order-md-1">
                    <form class="needs-validation" novalidate="">
                        <div class="row">
                            <div class="col-md-6 mb-6">
                                <label for="name">Name</label>
                                <input type="text" class="form-control {{$errors->has('name') ? 'is-danger' : ''}}" name="name"  value="{{$product->name}}">
                                <div class="invalid-feedback">
                                    Valid first name is required.
                                </div>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="status">Category</label>
                                <select class="form-control" name="category_id">

                                    @foreach($categories as $category)
                                        <option  value="{{$category->id}}" {{$product->category_id == $category->id ? 'selected' : ''}} >{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="size">Size</label>
                                <input type="text" class="form-control {{$errors->has('size') ? 'is-danger' : ''}}" name="size"  value="{{$product->size}}">
                                <div class="invalid-feedback">
                                    Valid last name is required.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="price">Price</label>
                                <input type="text" class="form-control {{$errors->has('price') ? 'is-danger' : ''}}" name="price"  value="{{$product->price}}">
                                <div class="invalid-feedback">
                                    Valid last name is required.
                                </div>
                            </div>

                        </div>

                        <hr class="mb-4">
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Edit product</button>
                    </form>
                </div>
            </div>
        </div>
    </form>
    @include('errors')
@endsection