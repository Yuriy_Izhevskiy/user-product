<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::resource('users', 'UsersController');

//Route::group(['middleware' => 'web', 'prefix' => 'users'], function () {
//    Route::get('/', 'UsersController@index')->name('users');
////    Route::get('/users/{id}', 'UsersController@show');
//    Route::get('/{id}/edit', 'UsersController@edit')->name('users.edit');
//    Route::patch('/{id}', 'UsersController@update')->name('users.update');
//});

Route::group(['middleware' => ['web']], function () {
    Route::get('/categories', 'CategoriesController@index')->name('categories');
    Route::get('/category-products/{category}', 'CategoryProductsController@show')->name('categories.show');
    Route::get('/products', 'ProductsController@show')->name('categories.show');
    Route::post('/categories', 'CategoriesController@store')->name('categories.store');

    Route::get('categories/{category}', 'CategoryProductsController@show');
    Route::patch('/categories/{category}', 'CategoriesController@update')->name('categories.update');
    Route::delete('categories/{category}', 'CategoriesController@destroy')->name('categories.destroy');


    Route::group(['middleware' => ['web', 'role:admin']], function () {
        Route::get('/categories/create', 'CategoriesController@create')->name('categories.create');
        Route::get('/categories/{category}/edit', 'CategoriesController@edit')->name('categories.edit');
    });

});

Route::group(['middleware' => ['web'], 'prefix' => 'dashboard'], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
});

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'products'], function () {
    Route::get('/', 'ProductsController@index')->name('products');
    Route::get('/create', 'ProductsController@create')->name('products.create');
    Route::post('/', 'ProductsController@store')->name('products.store');
    Route::get('/{product}/edit', 'ProductsController@edit')->name('products.edit');
    Route::patch('/{product}', 'ProductsController@update')->name('products.update');
    Route::delete('/{product}', 'ProductsController@destroy')->name('products.destroy');
});
