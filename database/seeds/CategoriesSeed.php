<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Category::class, 5)->create()
            ->each(function ($category) {
            info($category->name);
        });
    }
}
