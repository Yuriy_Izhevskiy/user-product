<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['name' => 'admin']);

        $permissionForAdmin[] = Permission::create(['name' => 'edit products']);
        $permissionForAdmin[] = Permission::create(['name' => 'delete products']);
        $permissionForAdmin[] = Permission::create(['name' => 'view products']);
        $permissionForAdmin[] = Permission::create(['name' => 'create products']);

        $permissionForAdmin[] = Permission::create(['name' => 'edit categories']);
        $permissionForAdmin[] = Permission::create(['name' => 'delete categories']);
        $permissionForAdmin[] = Permission::create(['name' => 'view categories']);
        $permissionForAdmin[] = Permission::create(['name' => 'create categories']);

        $role->syncPermissions($permissionForAdmin);

        $role = Role::create(['name' => 'regular']);



    }
}
