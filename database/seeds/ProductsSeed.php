<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Product::class, 80)->create()
            ->each(function ($category) {
                info($category->name);
            });


    }
}
