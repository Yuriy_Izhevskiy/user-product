<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'category_id' => function() {
            return App\Category::all()->random()->id;
        },
        'name' => (string)$faker->creditCardType,
        'status' => 1,
        'price' => mt_rand(15, 500),
        'size' => mt_rand(15, 50),

    ];
});
